<?php

$values = [
    "message" => "Hello world!",
    "count"   => 150,
    "pi"      => 3.14,
    "status"  => false,
    "result"  => null
];
//Definimos un arreglo con 5 elemento, pero cada uno de diferente tipo

$count = 3;
$price = 9.99;

$data = [$count, $price];

//Este es un arreglo bidimensional

$articles = [
    ["title" => "First post",   "content" => "This is the first post"],
    ["title" => "Another post", "content" => "Another post to read here"],
    ["title" => "Read this!",   "content" => "You must read this article!"]
];

var_dump($articles);
echo "<br>";
echo "<br>";
var_dump($articles[1]["title"]);//Another post
/*var_dump($articles[1][1]); Esto no se puede hacer, ya que dentro del arreglo, esta definido
por clave, no por posicion*/
