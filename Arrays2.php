<?php

/*En esta ocasión, si queremos acceder a una posicion del arreglo tiene que
ser mediante la clave*/

$articles = [
    "first"  => "First post",
    "second" => "Another post",
    "third"  => "Read this!"
];

var_dump($articles);
echo "<br>";
var_dump($articles["second"]);
