<?php

require 'Item11.php';
require 'Book11.php';

$my_item = new Item();


//echo $my_item->code;//No podemos acceder a code, por que es protected, solo se puede mediante la herencia
//Solo se puede mediante un get, es decir, una función que lo retorne.
echo $my_item->getCode();

$book = new Book();

echo $book->getCode();
