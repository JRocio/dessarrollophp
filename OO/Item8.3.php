<?php

class Item
{
    public CONST MAX_LENGTH = 24;//Constante para una clase

    public $name;

    public $description;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
}
