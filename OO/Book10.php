<?php

class Book extends Item //HERENCIA
{
    public $author;

    public function getListingDescription()//OVERIDING
    {
        return parent::getListingDescription() . " by " . $this->author; //
    }
}
