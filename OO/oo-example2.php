<?php


require 'Item.php';

$my_item = new Item();

$my_item->name = 'Example';
$my_item->description = 'A new description';
/*Le asigna ese atributo a este objeto, pero cuando creamos un nuevo objeto solo va a tener
los atributos declarados dentro de la clase, name y description */
$my_item->price = 2.99;

var_dump($my_item);

$my_item2 = new Item();
var_dump($my_item2);
