<?php

class Item
{
    public $name;//Es lo mismo que solo poner $name, por default es public

    protected $code = 1234;

    public function getListingDescription()
    {
        return "Item: " . $this->name;
    }
     public function getCode()
     {
       return $this->code;
     }
}
