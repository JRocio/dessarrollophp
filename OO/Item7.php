<?php

class Item
{
    public $name;

    public $description = 'This is the default';

    public static $count = 0;
    //Es un atributo general, en cualquier lugar que se modifique, se modifica para todos,
    //es propio de la clase, no de los objetos 

    public function __construct($name, $description) {
        $this->name = $name;
        $this->description = $description;

        static::$count++;
    }

    public function sayHello()
    {
        echo "Hello";
    }

    public function getName()
    {
        return $this->name;
    }

    public static function showCount()
    {
        echo static::$count;
    }
}
