//Copiar el archivo jquery-3.5.1.min en la misma carpeta en la que estará este archivo de las funciones
function eliminarProducto(id_producto)
{
  //enviamos request de manera asincrona
  //Manda un request a la url que ponemos dentro
  $.ajax({
    type :'POST',
    url:'php/eliminarProducto.php',
    data: {id : id_producto},//Datos que vamos a mandar
    cache: false,
    success: function(mi_respuesta){
       alert(mi_respuesta);//lanza resultado del request
       location.reload();//recarga la pagina
    }
  });
}

function formProducto(id_producto)
{
  window.location="php/edicionProducto.php?id="+id_producto;
}

function abre_nuevo_form()
{
  window.location="php/edicionProducto.php"
}
