<?php
//incluir archivo de conexion con la base de DATOS
 include("conexion.php");//Ponemos la ruta del archivo, pero como

//crear la consulta para listar DATOS
$consulta="SELECT
              id_prod,
              nombre,
              marca,
              precio,
              observaciones
              FROM prods";
    $ejecuta = $conexion->query($consulta) or die ("Error al consultar lista de productos<br> :".$conexion->error);
    /*variable donde se va a guardar el query que ejecutamos*/
?>
<div style="text-align:center;" >
<table id="lista_productos">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
    <th>Marca</th>
    <th>Precio</th>
    <th>Observaciones</th>
    <th colspan="2">Edicion</th><!--Un encabezado, dos divisiones de columna-->
  </tr>
  <?php
  /*Mientras encuentre registros en ejecuta, entonces los guarda uno en uno en resultados
  y los iteramos para mostrarlos*/
      while($resultados = $ejecuta->fetch_row())
      {
        echo '<tr>';
        echo '<td>'.$resultados[0].'</td>';
        echo '<td>'.$resultados[1].'</td>';
        echo '<td>'.$resultados[2].'</td>';
        echo '<td>'.$resultados[3].'</td>';
        echo '<td>'.$resultados[4].'</td>';
        echo '<td> <button type="button" onclick="formProducto('.$resultados[0].');">';
        echo 'Editar</button></td>';
        echo '<td> <button type="button" onclick="eliminarProducto('.$resultados[0].');">';
        echo 'Eliminar</button></td>';
        echo '</tr>';
      }
   ?>
</table>
</div>
