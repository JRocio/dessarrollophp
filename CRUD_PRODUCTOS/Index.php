<?php
?>


<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>CRUD PRODUCTS</title>
    <!--Incluimos libreria de JQuery-->
    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
    <!--Incluimos -->
    <script type="text/javascript" src="js/mainFunciones.js"></script>
  </head>
  <body>
    <!--Aqi listamos los productos-->
    <div id="contenido_lista">
      <?php include("php/listadoProductos.php");
      ?>
      </div>
    <!--Boton para agregar producto-->
    <button type="button" onclick="abre_nuevo_form();">Agregar</button>
  </body>
</html>

<style type="text/css">
   th{padding: 10px; background: rgba(225,0,0,.5); color: white;}
   /*  margen interno;*/

   td{background: rgba(0,0,0,.5); color:white; font-size: 15px;}

</style>
